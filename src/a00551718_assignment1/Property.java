/**
 * 
 */
package a00551718_assignment1;

import java.util.ArrayList;

/**
 * @author Jacqueline Leung
 *
 */
public class Property {
	private String propertyId;
	private String type;
	private double priceUsd;
	private Address address;
	private int numberOfBedrooms;
	private boolean swimmingPool;
	
	public static final int MIN_PROPERTY_ID_LENGTH = 1;
	public static final int MAX_PROPERTY_ID_LENGTH = 6;
	public static final String TYPE_ONE = "residence";
	public static final String TYPE_TWO = "commercial";
	public static final String TYPE_THREE = "retail";
	public static final int MIN_NUMBER_OF_BEDROOM = 1;
	public static final int MAX_NUMBER_OF_BEDROOMs = 20;
	
	/**
	 * @param propertyId
	 * @param type
	 * @param priceUsd
	 * @param address
	 * @param numberOfBedrooms
	 * @param swimmingPool
	 */
	public Property(String propertyId, String type, double priceUsd, Address address, int numberOfBedrooms,
			boolean swimmingPool) {
		if(propertyId == null) {
			throw new NullPointerException("Invalid property id: " + propertyId);
		}else if(propertyId.isBlank()) {
			throw new IllegalArgumentException("Invalid property id: " + propertyId);
		}else if(propertyId.length() < MIN_PROPERTY_ID_LENGTH || propertyId.length() > MAX_PROPERTY_ID_LENGTH) {
			throw new IllegalArgumentException("Invalid property id: " + propertyId);
		}
		
		if(type == null) {
			throw new NullPointerException("Invalid property type: " + type);
		}else if(type != null && !type.equalsIgnoreCase(TYPE_ONE) && !type.equalsIgnoreCase(TYPE_TWO)&& !type.equalsIgnoreCase(TYPE_THREE)){
			throw new IllegalArgumentException("Invalid property type: " + type);
		}
		
		if(priceUsd < 0) {
			throw new IllegalArgumentException("Invalid price: " + priceUsd);
		}
		
		if(address == null) {
			throw new NullPointerException("Invalid address: " + address);
		}
		
		if(numberOfBedrooms < MIN_NUMBER_OF_BEDROOM || numberOfBedrooms > MAX_NUMBER_OF_BEDROOMs) {
			throw new IllegalArgumentException("Invalid number of bedrooms: " + numberOfBedrooms);
		}
		
		this.propertyId = propertyId;
		this.type = type;
		this.priceUsd = priceUsd;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.swimmingPool = swimmingPool;
	}

	/**
	 * @return the priceUsd
	 */
	public double getPriceUsd() {
		return priceUsd;
	}

	/**
	 * @return the address
	 */
	public Address getAddress() {
		return address;
	}

	/**
	 * @param priceUsd the priceUsd to set
	 */
	public void setPriceUsd(double priceUsd) {
		this.priceUsd = priceUsd;
	}

	/**
	 * @return the propertyId
	 */
	public String getPropertyId() {
		return propertyId;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	
	/**
	 * @return the numberOfBedrooms
	 */
	public int getNumberOfBedrooms() {
		return numberOfBedrooms;
	}

	/**
	 * @return the swimmingPool
	 */
	public boolean hasSwimmingPool() {
		return swimmingPool;
	}

	public ArrayList<Property> getPropertyDetails() {
		// TODO Auto-generated method stub
		return null;
	}
}
