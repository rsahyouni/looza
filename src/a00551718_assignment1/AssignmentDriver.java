/**
 * 
 */
package a00551718_assignment1;

import java.util.ArrayList;

/**
 * @author Jacqueline Leung
 *
 */
public class AssignmentDriver {
	private final Address[] ADDRESS_DATA = new Address[12];
	private final Property[] PROPERTY_DATA = new Property[12];
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new AssignmentDriver().run();
	}
	
	public void run() {
		Agency agency = new Agency("BCIT");
		
		ADDRESS_DATA[0] = new Address("1a", 777, "56th avenue", "v7n2m8", "surrey");
		PROPERTY_DATA[0]= new Property("abc123", "residence", 499000.00, ADDRESS_DATA[0], 2, false);

		ADDRESS_DATA[1]= new Address(null, 123, "main street", "v7r2g2", "west vancouver");
		PROPERTY_DATA[1] = new Property("xyz789", "residence", 5999999.00, ADDRESS_DATA[1], 5, true);

	    ADDRESS_DATA[2] = new Address(null, 456, "elm street", "90210", "los angeles");
	    PROPERTY_DATA[2] = new Property("777def", "residence", 2500000.00, ADDRESS_DATA[2], 6, true);

	    ADDRESS_DATA[3] = new Address("44", 1111, "maple street", "v8y3r5", "vancouver");
	    PROPERTY_DATA[3] = new Property("876tru", "retail", 1000000.00, ADDRESS_DATA[3], 1, false);

	    ADDRESS_DATA[4] = new Address("9", 99, "gretzky way", "t6v7h3", "toronto");
	    PROPERTY_DATA[4] = new Property("9999", "commercial", 99999.00, ADDRESS_DATA[4], 1, false);

	    ADDRESS_DATA[5] = new Address("b", 711, "country road", "v8h5f5", "maple ridge");
	    PROPERTY_DATA[5] = new Property("mr6789", "residence", 740100.00, ADDRESS_DATA[5], 3, false);

	    ADDRESS_DATA[6] = new Address(null, 8785, "pinnacle avenue", "v9u3h3", "north vancouver");
	    PROPERTY_DATA[6] = new Property("78444a", "residence", 15000000.00, ADDRESS_DATA[6], 20, true);

	    ADDRESS_DATA[7] = new Address(null, 800, "elm street", "90557", "los angeles");
	    PROPERTY_DATA[7] = new Property("mmm33", "residence", 7100000.00, ADDRESS_DATA[7], 10, false);

	    ADDRESS_DATA[8] = new Address(null, 1515,"main street", "v8y7r3", "west vancouver");
	    PROPERTY_DATA[8] = new Property("678T", "commercial", 4000000.00, ADDRESS_DATA[8], 2, true);

	    ADDRESS_DATA[9] = new Address("6", 60, "60th street", "v8u9b1", "burnaby");
	    PROPERTY_DATA[9] = new Property("y6yyy", "retail", 700000.00, ADDRESS_DATA[9], 2, true);

	    ADDRESS_DATA[10] = new Address("7h", 1500, "railway avenue", "v9v5v4", "richmond");
	    PROPERTY_DATA[10] = new Property("A1212", "commercial", 840000.00, ADDRESS_DATA[10], 4, false);

	    ADDRESS_DATA[11] = new Address(null, 333, "elm street", "90111", "los angeles");
	    PROPERTY_DATA[11] = new Property("9000a", "residence", 1600000.00, ADDRESS_DATA[11], 3, false);
	    
	    agency.addProperty(PROPERTY_DATA[0]);
        agency.addProperty(PROPERTY_DATA[1]);
        agency.addProperty(PROPERTY_DATA[2]);
        agency.addProperty(PROPERTY_DATA[3]);
        agency.addProperty(PROPERTY_DATA[4]);
        agency.addProperty(PROPERTY_DATA[5]);
        agency.addProperty(PROPERTY_DATA[6]);
        agency.addProperty(PROPERTY_DATA[7]);
        agency.addProperty(PROPERTY_DATA[8]);
        agency.addProperty(PROPERTY_DATA[9]);
        agency.addProperty(PROPERTY_DATA[10]);
        agency.addProperty(PROPERTY_DATA[11]);
        
//        agency.getProperty("876tru");
        System.out.println("Testing Agency.getProperty(\"876tru\")~~~~~~");
        System.out.println("Property [" 
        + "priceUsd = "+ agency.getProperty("876tru").getPriceUsd()
        + ", address= [unitNumber= " + agency.getProperty("876tru").getAddress().getUnitNumber()
        + ", streetNumber = " + agency.getProperty("876tru").getAddress().getStreetNumber()
        + ", streetName = " + agency.getProperty("876tru").getAddress().getStreetName()
        + ", postalCode = " + agency.getProperty("876tru").getAddress().getPostalCode()
        + ", city = " + agency.getProperty("876tru").getAddress().getCity() + "]"
        + ", numberOfBedrooms = " + agency.getProperty("876tru").getNumberOfBedrooms()
        + ", swimmingPool = " + agency.getProperty("876tru").hasSwimmingPool()
        + ", type = " + agency.getProperty("876tru").getType()
        + ", propertyId = " + agency.getProperty("876tru").getPropertyId() + "]" + "\n");
        
        System.out.println("Testing Agency.getTotalPropertyValues()~~~~~~");
        String value = String.format("%.2f", agency.getTotalPropertyValues());
        System.out.println("Total property values are: $" + value + "\n");
        
        System.out.println("Testing Agency.getPropertiesWithPools()~~~~~~");
        System.out.println("Here is a list of properties with pools: ");
//      System.out.println(agency.getPropertiesWithPools().size());
        int index = 0;
        for(Property p: agency.getPropertiesWithPools()) {
        	System.out.println("Property [" 
        	        + "priceUsd = "+ agency.getPropertiesWithPools().get(index).getPriceUsd()
        	        + ", address= [unitNumber= " + agency.getPropertiesWithPools().get(index).getAddress().getUnitNumber()
        	        + ", streetNumber = " + agency.getPropertiesWithPools().get(index).getAddress().getStreetNumber()
        	        + ", streetName = " + agency.getPropertiesWithPools().get(index).getAddress().getStreetName()
        	        + ", postalCode = " + agency.getPropertiesWithPools().get(index).getAddress().getPostalCode()
        	        + ", city = " + agency.getPropertiesWithPools().get(index).getAddress().getCity() + "]"
        	        + ", numberOfBedrooms = " + agency.getPropertiesWithPools().get(index).getNumberOfBedrooms()
        	        + ", swimmingPool = " + agency.getPropertiesWithPools().get(index).hasSwimmingPool()
        	        + ", type = " + agency.getPropertiesWithPools().get(index).getType()
        	        + ", propertyId = " + agency.getPropertiesWithPools().get(index).getPropertyId() + "]");
        	        index++;        	        
        }
        System.out.println("\n");
        
        ArrayList<Address> results= new ArrayList<Address>();
        results = agency.getPropertiesOn("elm street");
        int resultIndex = 0;
        System.out.println("Testing Agency.getPropertiesOn(\"elm street\")~~~~~~");
        System.out.println("Here is a list of properties on Elm Street");
        for(Address a: results) {
        	System.out.println("Address[unitNumber= " + results.get(resultIndex).getUnitNumber()
        	        + ", streetNumber = " + results.get(resultIndex).getStreetNumber()
        	        + ", streetName = " + results.get(resultIndex).getStreetName()
        	        + ", postalCode = " + results.get(resultIndex).getPostalCode()
        	        + ", city = " + results.get(resultIndex).getCity() + "]");
        			resultIndex++;        	        
        }
        System.out.println("\n");

        Property[] propertiesBetweenResults = new Property[20];
        propertiesBetweenResults = agency.getPropertiesBetween(500000.00,999999.00);

        System.out.println("Testing Agency.getPropertiesBetween(minUsd, maxUsd)~~~~~~");
        System.out.println("Here is a list of properties between $500000.00 and $999999.00");
        int pbIndex = 0;
//        for(int pbIndex = 0; pbIndex <= propertiesBetweenResults.length; pbIndex++) {
//        	System.out.println("Property [" 
//            	        + "priceUsd = "+ propertiesBetweenResults[pbIndex].getPriceUsd()
//            	        + ", address= [unitNumber= " + propertiesBetweenResults[pbIndex].getAddress().getUnitNumber()
//            	        + ", streetNumber = " + propertiesBetweenResults[pbIndex].getAddress().getStreetNumber()
//            	        + ", streetName = " +propertiesBetweenResults[pbIndex].getAddress().getStreetName()
//            	        + ", postalCode = " + propertiesBetweenResults[pbIndex].getAddress().getPostalCode()
//            	        + ", city = " + propertiesBetweenResults[pbIndex].getAddress().getCity() + "]"
//            	        + ", numberOfBedrooms = " + propertiesBetweenResults[pbIndex].getNumberOfBedrooms()
//            	        + ", swimmingPool = " + propertiesBetweenResults[pbIndex].hasSwimmingPool()
//            	        + ", type = " + propertiesBetweenResults[pbIndex].getType()
//            	        + ", propertyId = " + propertiesBetweenResults[pbIndex].getPropertyId() + "]");
////        	System.out.println(propertiesBetweenResults[pbIndex]);
//			pbIndex++;    
//
//        }
        
        for(Property pbr: propertiesBetweenResults) {
        	System.out.println("Property [" 
            	        + "priceUsd = "+ propertiesBetweenResults[pbIndex].getPriceUsd()
            	        + ", address= [unitNumber= " + propertiesBetweenResults[pbIndex].getAddress().getUnitNumber()
            	        + ", streetNumber = " + propertiesBetweenResults[pbIndex].getAddress().getStreetNumber()
            	        + ", streetName = " +propertiesBetweenResults[pbIndex].getAddress().getStreetName()
            	        + ", postalCode = " + propertiesBetweenResults[pbIndex].getAddress().getPostalCode()
            	        + ", city = " + propertiesBetweenResults[pbIndex].getAddress().getCity() + "]"
            	        + ", numberOfBedrooms = " + propertiesBetweenResults[pbIndex].getNumberOfBedrooms()
            	        + ", swimmingPool = " + propertiesBetweenResults[pbIndex].hasSwimmingPool()
            	        + ", type = " + propertiesBetweenResults[pbIndex].getType()
            	        + ", propertyId = " + propertiesBetweenResults[pbIndex].getPropertyId() + "]");
//        	System.out.println(propertiesBetweenResults[pbIndex]);
			pbIndex++;    

        }
        System.out.println("\n");
        
        

//        agency.getPropertiesOn("elm street");
//        agency.getPropertiesWithBedrooms(2, 4);
//        agency.getPropertiesOfType("residence");
//        agency.getPropertiesOfType("commercial");
//        agency.getPropertiesOfType("retail");
        
        
	}  

}
