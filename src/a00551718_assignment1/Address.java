package a00551718_assignment1;

/**
 * @author Jacqueline Leung
 *
 */
public class Address {
	private String unitNumber;
	private int streetNumber;
	private String streetName;
	private String postalCode;
	private String city;
	
	public static final int MIN_UNIT_NUMBER_LENGTH = 1;
	public static final int MAX_UNIT_NUMBER_LENGTH = 4;	
	public static final int MIN_STREET_NUMBER = 1;
	public static final int MAX_STREET_NUMBER = 999999;
	public static final int MIN_STREET_NAME_LENGTH = 1;
	public static final int MAX_STREET_NAME_LENGTH = 20;
	public static final int MIN_POSTAL_CODE_LENGTH = 5;
	public static final int MAX_POSTAL_CODE_LENGTH = 6;
	public static final int MIN_CITY_LENGTH = 1;
	public static final int MAX_CITY_LENGTH = 30;
	
	/**
	 * @param unitNumber
	 * @param streetNumber
	 * @param streetName
	 * @param postalCode
	 * @param city
	 */
	public Address(String unitNumber, int streetNumber, String streetName, String postalCode, String city) {
		if(unitNumber != null && unitNumber.isBlank()){
			throw new IllegalArgumentException("Invalid unit number: ");
		}else if(unitNumber != null && unitNumber.length() < MIN_UNIT_NUMBER_LENGTH){
			throw new IllegalArgumentException("Invalid unit number: " + unitNumber);
		}else if (unitNumber != null && unitNumber.length() > MAX_UNIT_NUMBER_LENGTH) {
			throw new IllegalArgumentException("Invalid unit number: " + unitNumber);
		}
		
		if(streetNumber < MIN_STREET_NUMBER) {
			throw new IllegalArgumentException("Invalid street number: " + streetNumber);
		}else if(streetNumber > MAX_STREET_NUMBER) {
			throw new IllegalArgumentException("Invalid street number: "+ streetNumber);
		}		
		
		if(streetName == null) {
			throw new NullPointerException("Invalid street name: " + streetName);
		}else if(streetName != null && streetName.isBlank()) {
			throw new IllegalArgumentException("Invalid street name: ");
		}else if(streetName != null && streetName.length() < MIN_STREET_NAME_LENGTH) {
			throw new IllegalArgumentException("Invalid street name: " + streetName);
		}else if(streetName != null && streetName.length() > MAX_STREET_NAME_LENGTH ){
			throw new IllegalArgumentException("Invalid street name: " + streetName);			
		}	
		
		
		if(postalCode == null) {
			throw new NullPointerException("Invalid postal code: " + postalCode);
		}else if(postalCode != null && postalCode.isBlank()) {
			throw new IllegalArgumentException("Invalid postal code: " + postalCode);
		}else if(postalCode != null && postalCode.length() < MIN_POSTAL_CODE_LENGTH) {
			throw new IllegalArgumentException("Invalid postal code: " + postalCode);
		}else if(postalCode != null && postalCode.length() > MAX_POSTAL_CODE_LENGTH){
			throw new IllegalArgumentException("Invalid postal code: " + postalCode);
		}
					
		
		if(city == null) {
			throw new NullPointerException("Invalid city: " + city);
		}else if(city != null && city.isBlank()){
			throw new IllegalArgumentException("Invalid city: ");
		}else if(city != null && city.length() < MIN_CITY_LENGTH){
			throw new IllegalArgumentException("Invalid city: " + city);
		}else if(city != null && city.length() > MAX_CITY_LENGTH){
			throw new IllegalArgumentException("Invalid city: " + city);
		}
		
		this.unitNumber = unitNumber;
		this.streetNumber = streetNumber;
		this.streetName = streetName;
		this.postalCode = postalCode;
		this.city = city;
		
	}

	/**
	 * @return the unitNumber
	 */
	public String getUnitNumber() {
		if(unitNumber == null){
			return null;
		}else {
			return unitNumber;
		}		
	}

	/**
	 * @return the streetNumber
	 */
	public int getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName() {
		
		return streetName;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}	

}
