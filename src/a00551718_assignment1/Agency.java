/**
 * 
 */
package a00551718_assignment1;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


/**
 * @author Jacqueline Leung
 *
 */
public class Agency {
	private String name;
	private HashMap<String, Property> properties;
	
	public static final int MIN_NAME_LENGTH = 1;
	public static final int MAX_NAME_LENGTH = 30;
	
	/**
	 * @param name
	 */
	public Agency(String name) {
		if(name == null || name.isBlank() || name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH) {
			throw new IllegalArgumentException("Invalid name.");
		}
			
		this.setName(name);
				
		properties = new HashMap<>();
	}
	
	public void addProperty(Property property) {
		
		if(property != null) {
			properties.put(property.getPropertyId(), property);			
		}
//		System.out.println(properties.size());
		
//		for(String i: properties.keySet()) {
//			System.out.println(properties.get(i).getPropertyId()
//					+ properties.get(i).getType()
//					+ properties.get(i).getPriceUsd()
//					+ properties.get(i).getAddress().getUnitNumber()
//					+ properties.get(i).getAddress().getStreetNumber()
//					+ properties.get(i).getAddress().getStreetName()
//					+ properties.get(i).getAddress().getPostalCode()
//					+ properties.get(i).getAddress().getCity()
//					+ properties.get(i).getNumberOfBedrooms()
//					+ properties.get(i).hasSwimmingPool());   id.equalsIgnoreCase(propertyId)
		
	}
	
	public void removeProperty(String propertyId) {
		
		if(propertyId != null) {
			for(String id: properties.keySet()) {
				if(id.equalsIgnoreCase(propertyId) || id.equals(propertyId)) {
					properties.remove(id);
				}				
			}				
		}		
	}
	
	public Property getProperty(String propertyId) {
		Property matchProperty = null;
		
		Set<String> keys = properties.keySet();
		for(String key: keys) {
			if(key.equalsIgnoreCase(propertyId)) {
				matchProperty = properties.get(key);
			}
		}
		
//		for(String id: properties.keySet()) {
//			if(id.equalsIgnoreCase(propertyId)) {
//				matchProperty = properties.get(id);				
//			}else {
//				return null;
//			}
//		}		
		return matchProperty;
	}
	
	public double getTotalPropertyValues() {
		double TotalPropertyValue = 0;
		
		for(Property i: properties.values()) {
			TotalPropertyValue += i.getPriceUsd();
		}
		return TotalPropertyValue;
	}
	
	
	public ArrayList<Property> getPropertiesWithPools() {
		ArrayList<Property> PropertiesWithPools = new ArrayList<Property>();
		
		for(Property i: properties.values()) {
			if(i.hasSwimmingPool() == true) {
				PropertiesWithPools.add(i);
			}
		}		
		return PropertiesWithPools;
	}
	
	public Property[] getPropertiesBetween(double minUsd, double maxUsd) {
		
		int index = 0;
		int counter = 0;
		
		for(Property i: properties.values()) {
			if(i.getPriceUsd() >= minUsd && i.getPriceUsd() <= maxUsd) {
				counter++;
			}
		}
		
		Property[] propertiesBetween = new Property[counter];

		for(Property i: properties.values()) {
			if(i.getPriceUsd() >= minUsd && i.getPriceUsd() <= maxUsd) {
				propertiesBetween[index] = i;
				index++;
			}	
		}
		
		return propertiesBetween;
	}
	
	public ArrayList<Address> getPropertiesOn(String streetName){
		ArrayList<Address> propertiesOn= new ArrayList<Address>();
		
		for(Property i: properties.values()) {
			if(i.getAddress().getStreetName().equalsIgnoreCase(streetName)) {
				propertiesOn.add(i.getAddress());				
			}
		}
		return propertiesOn;
	}
	
	public HashMap<String, Property> getPropertiesWithBedrooms(int minBedrooms, int maxBedrooms){
		HashMap<String, Property> propertiesWithBedrooms = new HashMap<>();
		
		for(Property i: properties.values()) {
			if(i.getNumberOfBedrooms()>= minBedrooms || i.getNumberOfBedrooms() < maxBedrooms) {
				propertiesWithBedrooms.put(i.getPropertyId(), i);
			}
		}
		return propertiesWithBedrooms;
	}
	
	public ArrayList<Property> getPropertiesOfType(String propertyType){
		ArrayList<Property> propertiesType = new ArrayList<Property>();
		
		for(Property i: properties.values()) {
			if(i.getType().equalsIgnoreCase(propertyType) && i.hasSwimmingPool() == true) {
				propertiesType.add(i);
			}
		}
		return propertiesType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
